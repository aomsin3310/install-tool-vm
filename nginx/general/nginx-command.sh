#!/bin/bash

# Install NGINX
sudo apt install nginx -y
sleep 2

# Navigate to NGINX Configuration Directory
cd /etc/nginx/
sleep 2

# Edit NGINX Configuration File
# Alternatively, you can use nano:
# sudo nano nginx.conf

# Add the following line to nginx.conf to include additional configuration files
echo 'include /etc/nginx/default.d/*.conf;' | sudo tee -a nginx.conf
sleep 2
# Create a conf.d directory if it doesn't exist
sudo mkdir -p conf.d
sleep 2
# Create or edit the default.conf file within conf.d
# Alternatively, you can use nano:
# sudo nano /etc/nginx/conf.d/default.conf

# Add the NGINX reverse proxy configuration to default.conf
echo '
server {
    listen          80 default_server;
    listen          [::]:80 default_server;
    server_name     aomsin.co.th;
    server_name     _;

    # Router
    location / {
        proxy_pass                      http://127.0.0.1:5080;
        proxy_set_header                Host $host;
        proxy_set_header                X-Real-IP $remote_addr;
        proxy_set_header                X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header                X-Forwarded-Proto $scheme;
    }
}' | sudo tee /etc/nginx/conf.d/default.conf
sleep 2
# Enable NGINX on startup
sudo systemctl enable nginx
sleep 2
# Test NGINX configuration
sudo nginx -t
sleep 2
# Reload NGINX configuration
sudo systemctl reload nginx
sleep 2