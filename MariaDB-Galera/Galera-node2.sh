#!/bin/bash

# Update and upgrade packages
sudo apt update
sleep 2

sudo apt upgrade -y
sleep 2

# Install MariaDB server
sudo apt install mariadb-server -y
sleep 2

# Set root password non-interactively
sudo mysql -u root <<EOF
ALTER USER 'root'@'localhost' IDENTIFIED BY 'viewOn1234_!';
FLUSH PRIVILEGES;
EOF
sleep 2

# MySQL Galera Cluster configuration
config_file="/etc/mysql/conf.d/galera.cnf"
config_content="[mysqld]
binlog_format=ROW
default-storage-engine=innodb
innodb_autoinc_lock_mode=2
bind-address=0.0.0.0
# Galera Provider Configuration
wsrep_on=ON
wsrep_provider=/usr/lib/galera/libgalera_smm.so
# Galera Cluster Configuration
wsrep_cluster_name=\"test_cluster\"
wsrep_cluster_address=\"gcomm://157.245.149.171,143.198.93.140\"
# Galera Synchronization Configuration
wsrep_sst_method=rsync
# Galera Node Configuration
wsrep_node_address=\"143.198.93.140\"
wsrep_node_name=\"NODE2\""

echo "$config_content" | sudo tee "$config_file" > /dev/null

echo "MySQL configuration file created at $config_file"
sleep 2

# Configure firewall
sudo ufw status
sleep 2

sudo ufw allow from *ip*
sleep 2

sudo ufw allow 3306,4567,4568,4444/tcp
sleep 2

sudo ufw allow 4567/udp
sleep 2

# Stop MySQL service
sudo systemctl stop mysql
sleep 2

# Check MySQL service status
sudo systemctl status mysql
sleep 2

# Start Galera Cluster
sudo systemctl start mysql
sleep 2
