#!/bin/bash
cat <<EOF
==================================
Kickstart bash script for Ubuntu
by Patcharapong Prohmwichai
Version 1.0
==================================
EOF
# Exit if not support linux distro
if [[ $(lsb_release -cs) != "focal" && $(lsb_release -cs) != "jammy" ]]; then
  echo "Current Ubuntu distro" $(lsb_release -cs) "not support yet."
  exit 1
fi

# Prerequisite script must run as root
[ $(id -u) -ne 0 ] && { echo 'Run this script with root privileges.'; exit 1; } || echo 'Running as root, starting service ... '

# Condition check
check_result() {
  if [ $1 == 0 ]; then
    echo "ok"
  else
    echo "failed"
    exit $1
  fi
}

valid_cidr() {
  CIDR="$1"
  if [[ $CIDR =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\/[0-9]+$ ]]; then
    # Parse "a.b.c.d/n" into five separate variables
    IFS="./" read -r ip1 ip2 ip3 ip4 N <<< "$CIDR"
    # Convert IP address from quad notation to integer
    ip=$(($ip1 * 256 ** 3 + $ip2 * 256 ** 2 + $ip3 * 256 + $ip4))
    if [ $N -gt 32 ]; then
      return 1
    fi
    # Remove upper bits and check that all $N lower bits are 0
    if [ $(($ip % 2**(32-$N))) = 0 ]; then
      return 0 # CIDR OK!
    else
      return 1 # CIDR NOT OK!
    fi
  fi
  return 1
}

get_innodb_cache() {
  _mem=$(grep MemTotal /proc/meminfo | awk '{print $2}')
  _suffix=M
  if [ $_mem -gt 1000000 ]; then
    _mem=$(($_mem*80/100000000))
    _suffix=G
  else
    _mem=$(($_mem*80/100000))
    _suffix=G
  fi
  echo $_mem$_suffix
}

# INET infra or not
_inet_infra=0
while true; do
  read -p "Is this INET server? (y/n) [default: y] " yn
  [ ! $yn ] && yn=y
  case $yn in 
    [yY] ) _inet_infra=1;
      break;;
    [nN] ) _inet_infra=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

# Ask for extended install

# - init-system
_init_system=0
while true; do
  read -p "Do you want to install new system configuration? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _init_system=1;
      break;;
    [nN] ) _init_system=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

# - docker-ce
_install_docker=0
while true; do
  read -p "Do you want to install docker-ce? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _install_docker=1;
      break;;
    [nN] ) _install_docker=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

# - node_exporter
_node_exporter=0
while true; do
  read -p "Do you want to install node_exporter? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _node_exporter=1;
      break;;
    [nN] ) _node_exporter=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

# - nginx
_nginx=0
while true; do
  read -p "Do you want to install nginx? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _nginx=1;
      break;;
    [nN] ) _nginx=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

# - nfs_server
_nfs_server=0
_nfs_dir=
_nfs_net=
while true; do
  read -p "Do you want to install nfs_server? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _nfs_server=1;
      break;;
    [nN] ) _nfs_server=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done
# Ask for more information if nfs server is on
if [ $_nfs_server -eq 1 ]; then
  while true; do
    read -p "NFS export dir [default: /storage]: " nfs_dir
    [ ! $nfs_dir ] && nfs_dir=/storage
    if [ ! -d $nfs_dir ]; then
      echo Destination dir not exist, creating
      sudo mkdir -p $nfs_dir > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        _nfs_dir=$nfs_dir
        break
      else
        echo Cannot create new dir, try again.
      fi
    else 
      _nfs_dir=$nfs_dir
      echo Destination dir already existed, do nothing.
      break
    fi
  done

  while true; do
    read -p "NFS export to network [default: 192.168.1.0/24]: " nfs_net
    [ ! $nfs_net ] && nfs_net="192.168.1.0/24"
    valid_cidr $nfs_net
    if [ $? -eq 0 ]; then
      _nfs_net=$nfs_net
      break
    else
      echo Invalid network address, try again
    fi 
  done
fi

# - mariadb
_mariadb=0
_mariadb_innodb_cache=$(get_innodb_cache)
_mariadb_galera_on=0
_mariadb_galera_cluster_name=
while true; do
  read -p "Do you want to install MariaDB server? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _mariadb=1;
      break;;
    [nN] ) _mariadb=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done
# Ask for more information
if [ $_mariadb -eq 1 ]; then
  while true; do
    read -p "Do you want to enable galera cluster? (y/n) [default: n] " yn
    [ ! $yn ] && yn=n
    case $yn in 
      [yY] ) _mariadb_galera_on=1;
        break;;
      [nN] ) _mariadb_galera_on=0;
        break;;
      * ) echo invalid response;
        exit 1;;
    esac
  done
  if [ $_mariadb_galera_on -eq 1 ]; then
    while true; do
      read -p "Input cluster name: " cluster_name
      if [[ $cluster_name =~ ^[a-zA-Z0-9\_\-]+$ ]]; then
        _mariadb_galera_cluster_name=$cluster_name
        break
      else
        echo invalid cluster name, try again.
      fi
    done    
  fi
fi

# add new user
_add_user=0
_username=
_password=
while true; do
  read -p "Do you want to add new user? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _add_user=1;
      break;;
    [nN] ) _add_user=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done
if [ $_add_user -eq 1 ]; then
  while true; do
    read -p "Username: " username
    if [[ $username =~ ^[a-zA-Z0-9]+$ ]]; then
      _username=$username
      break
    else
      echo invalid username, try again.
    fi
  done

  while true; do
    read -s -p "Password: " password
    [ $password ] && _password=$password; echo ""; break || echo you should input some password
  done
fi

# reboot
_reboot=0
while true; do
  read -p "Do you want to reboot system after finish script? (y/n) [default: y] " yn
  [ ! $yn ] && yn=y
  case $yn in 
    [yY] ) _reboot=1;
      break;;
    [nN] ) _reboot=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

echo "============ Install summary ============"
echo ">> Hostname:" $(hostname)
echo ">> Current user:" $(whoami)
_ubuntu_release=$(lsb_release -cs)
# focal = 20.04
# jammy = 22.04
echo ">> Ubuntu release:" $_ubuntu_release
[ $_inet_infra -eq 1 ] && echo ">> Server is in INET" || echo ">> Server is outside INET"
[ $_init_system -eq 1 ] && echo ">> Install new system"
[ $_install_docker -eq 1 ] && echo ">> Install Docker Community version 20.10"
[ $_node_exporter -eq 1 ] && echo ">> Install node_exporter service"
[ $_nginx -eq 1 ] && echo ">> Install NGINX 1.25.2 and custom module"
[ $_nfs_server -eq 1 ] && echo ">> Install NFS Server to dir $_nfs_dir and scope only $_nfs_net"
[ $_mariadb -eq 1 ] && echo ">> Install MariaDB Server 10.6 with InnoDB Cache size: $_mariadb_innodb_cache"
[ $_mariadb_galera_on -eq 1 ] && echo ">> Enable MariaDB Galera cluster: $_mariadb_galera_cluster_name"
[ $_add_user -eq 1 ] && echo ">> Add new user $_username"
[ $_reboot -eq 1 ] && echo ">> Reboot system after finish script"

# Confirm install
_confirm=0
while true; do
  read -p "Confirm and coninue install? (y/n) [default: n]" yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _confirm=1;
      break;;
    [nN] ) exit 0;;
    * ) echo invalid response;
      exit 1;;
  esac
done

# Start script and stamp time
echo "============ Starting automation script ============"
StartTime="$(date -u +%s.%N)"

# Selected new system
if [ $_init_system -eq 1 ]; then
  echo "============ Initialize new server ============"
  # hold docker-ce and nginx to prevent malform
  sudo apt-mark hold docker-ce nginx > /dev/null 2>&1
  # Update and install upgrade package
  echo ">> Update and upgrade system ... "
  sudo apt update && sudo apt upgrade -y
  sudo apt-mark unhold docker-ce nginx > /dev/null 2>&1

  # Install neccessary packages
  echo -n ">> Installing neccessary packages ... "
  sudo apt install -y net-tools git ntpdate nfs-common bc > /dev/null 2>&1
  check_result $?

  # Sync date/time
  echo -n ">> Synchronizing time to pool.ntp.org server ... "
  sudo ntpdate pool.ntp.org > /dev/null 2>&1
  check_result $?

  # Setup auto synchronize date/time every 6 hours
  echo -n ">> Adding crontab to auto synchronize date/time ... "
  # Check existed config
  if ! grep -Fxq "### Auto Sync NTP ###" /etc/crontab; then
    cat <<EOF | sudo tee -a /etc/crontab > /dev/null 2>&1
### Auto Sync NTP ###
0 */6 * * *	root	/usr/sbin/ntpdate pool.ntp.org
EOF
    check_result $?
  else
    echo "ok"
  fi

  # Secure SSH service
  echo -n ">> Securing SSH Service - Disable permit root login ... "
  sudo sed -i "s/#PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config > /dev/null 2>&1
  check_result $?
  echo -n ">> Securing SSH Service - Disable password authentication ... "
  sudo sed -i "s/PasswordAuthentication yes/PasswordAuthentication no/g" /etc/ssh/sshd_config > /dev/null 2>&1
  check_result $?
  echo -n ">> Configure SSH Service - Disable UseDNS to prevent reverse lookup ... "
  sudo sed -i "s/#UseDNS yes/UseDNS no/g" /etc/ssh/sshd_config > /dev/null 2>&1
  check_result $?
  echo -n ">> Configure SSH Service - Disable X11Forwarding ... "
  sudo sed -i "s/X11Forwarding yes/X11Forwarding no/g" /etc/ssh/sshd_config > /dev/null 2>&1
  check_result $?

  # Set ssh key
  if [ ! -d ~/.ssh ]; then
    echo -n ">> Make ~/.ssh dir ... "
    mkdir -p ~/.ssh > /dev/null 2>&1
    check_result $?
  fi
  if [ ! -f ~/.ssh/authorized_keys ]; then
    echo -n ">> Add ssh public key to ~/.ssh/authorized_keys file ... "
    curl -ksS https://kb.sdi.one.th/repo/files/ubuntu/id.pub | tee -a ~/.ssh/authorized_keys > /dev/null 2>&1
    check_result $?
  fi

  # Change location time
  echo -n ">> Setup localtime to Asia/Bangkok ... "
  sudo timedatectl set-timezone Asia/Bangkok > /dev/null 2>&1
  check_result $?

  # Disable systemd-resolved
  echo -n ">> Disable systemd-resolved.service ... "
  sudo systemctl disable systemd-resolved.service > /dev/null 2>&1
  check_result $?

  echo -n ">> Stop systemd-resolved.service ... "
  sudo systemctl stop systemd-resolved > /dev/null 2>&1
  check_result $?

  # Clear old resolve.conf file
  _dns=$(cat <<EOF
nameserver 203.150.213.1
nameserver 203.150.218.161
EOF
  )
  if [ $_inet_infra -eq 0 ]; then
  _dns=$(cat <<EOF
nameserver=8.8.8.8
nameserver=8.8.4.4
EOF
  )
  fi

  # Set dns to resolve file
  echo -n ">> Set /etc/resolv.conf to default setup ... "
  sudo rm -rf /etc/resolv.conf > /dev/null 2>&1
  cat <<EOF | sudo tee /etc/resolv.conf > /dev/null 2>&1
nameserver 203.150.213.1
nameserver 203.150.218.161
EOF
  if [ $_inet_infra -ne 1 ]; then
    cat <<EOF | sudo tee /etc/resolv.conf > /dev/null 2>&1
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF
  fi
  check_result $?

# End init system
fi

# set ssh key to new user
if [ $_add_user -eq 1 ]; then
  echo "============ Add new user: $_username ============"
  if [ $(whoami) != $_add_user ]; then
    if ! grep -Fq $_username /etc/passwd; then
      echo -n ">> Create new user $_username ... "
      sudo adduser --disabled-password --gecos "" $_username > /dev/null 2>&1
      check_result $?
    fi

    echo -n ">> Reset user $_username password ... "
    echo "$_username:$_password" | sudo chpasswd > /dev/null 2>&1
    check_result $?

    if [ ! -d /home/$_username/.ssh ]; then
      echo -n ">> Make /home/$_username/.ssh dir ... "
      mkdir -p /home/$_username/.ssh > /dev/null 2>&1
      check_result $?
    fi

    if [ ! -f /home/$_username/.ssh/authorized_keys ]; then
      echo -n ">> Add ssh public key to /home/$_username/.ssh/authorized_keys file ... "
      if [ -s ~/.ssh/authorized_keys ]; then
        cat ~/.ssh/authorized_keys | tee -a /home/$_username/.ssh/authorized_keys > /dev/null 2>&1
      else
        curl -ksS https://kb.sdi.one.th/repo/files/ubuntu/id.pub | tee -a /home/$_username/.ssh/authorized_keys > /dev/null 2>&1
      fi
      check_result $?
    fi

    echo -n ">> Add $_username to sudoers group ... "
    sudo usermod -aG sudo $_username > /dev/null 2>&1
    check_result $?
  fi
fi

# Selected to install docker
if [ $_install_docker -eq 1 ]; then
  echo "============ Install Docker-CE 20.10 ============"
  echo -n ">> Add docker gpg key ... "
  [ ! -d /usr/share/keyrings ] && sudo mkdir -p /usr/share/keyrings > /dev/null 2>&1
  curl -kfsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -q --yes -o /usr/share/keyrings/docker-archive-keyring.gpg > /dev/null 2>&1
  check_result $?

  echo -n ">> Add docker package repositories ... "
  echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null 2>&1
  check_result $?

  # Install docker with specific version
  echo -n ">> Update new package repositories ... "
  sudo apt update

  echo -n ">> Unhold docker-ce version ... "
  sudo apt-mark unhold docker-ce > /dev/null 2>&1
  check_result $?

  _docker_version=$(sudo apt-cache madison docker-ce | grep 20.10 | head -n 1 | awk -F '|' '{print $2}' | xargs)
  echo -n ">> Install docker-ce version $_docker_version ... "
  sudo apt install --allow-downgrades -q -y docker-ce=$_docker_version

  echo -n ">> Hold docker-ce to prevent docker update to latest version ... "
  sudo apt-mark hold docker-ce > /dev/null 2>&1
  check_result $?

  # enable docker on startup
  echo -n ">> Enable docker service on system startup ... "
  sudo systemctl enable docker > /dev/null 2>&1
  check_result $?

  # Add registry mirror
  echo -n ">> Add local docker registry ... "
  cat <<EOF | sudo tee /etc/docker/daemon.json > /dev/null 2>&1
{
  "registry-mirrors": ["https://img.sdi.one.th"]
}
EOF
  check_result $?

  # Disable system swap
  echo -n ">> Disable swap all ... "
  sudo swapoff -a > /dev/null 2>&1
  check_result $?

  echo -n ">> Change /etc/fstab to disable swap on startup ... "
  sudo sed -ri '/\sswap\s/s/^#?/#/' /etc/fstab > /dev/null 2>&1
  check_result $?

  # pre-pare docker parameter
  echo -n ">> Setup docker parameter ... "
  cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf > /dev/null 2>&1
overlay
br_netfilter
EOF
  check_result $?

  sudo modprobe overlay > /dev/null 2>&1
  sudo modprobe br_netfilter > /dev/null 2>&1

  # sysctl params required by setup, params persist across reboots
  echo -n ">> Setup net bridge-nf for kubenetes ... "
  cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf > /dev/null 2>&1
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
  check_result $?

  # Apply sysctl params without reboot
  sudo sysctl --system > /dev/null 2>&1

  echo -n ">> Append current to docker group ... "
  sudo usermod -aG docker $USER > /dev/null 2>&1
  check_result $?
fi

# Selected to install node_exporter
if [ $_node_exporter -eq 1 ]; then
  echo "============ Install node_exporter 1.15 ============"
  # check node_exporter already installed
  sudo systemctl status node_exporter > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo -n ">> Download and extract node_exporter ... "
    curl -kfsSL https://kb.sdi.one.th/repo/files/ubuntu/node_exporter.tar.gz | sudo tar xz --directory /opt > /dev/null 2>&1
    check_result $?

    echo -n ">> Add node_exporter service file ... "
    cat <<EOF | sudo tee /etc/systemd/system/node_exporter.service > /dev/null 2>&1
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=root
Group=root
Type=simple
ExecStart=/opt/node_exporter/node_exporter

[Install]
WantedBy=multi-user.target
EOF
    check_result $?

    echo -n ">> Refresh systemctl daemon ... "
    sudo systemctl daemon-reload > /dev/null 2>&1
    check_result $?

    echo -n ">> Enable node_exporter service ... "
    sudo systemctl enable node_exporter > /dev/null 2>&1
    check_result $?
  fi
fi

# Selected to install nginx
if [ $_nginx -eq 1 ]; then
  echo "============ Install NGINX 1.25.2 & custom module ============"
  echo -n ">> Add nginx repository gpg key ... "
  [ ! -d /usr/share/keyrings ] && sudo mkdir -p /usr/share/keyrings > /dev/null 2>&1
  curl -kfsSL https://nginx.org/keys/nginx_signing.key | sudo gpg --dearmor -q --yes -o /usr/share/keyrings/nginx-archive-keyring.gpg > /dev/null 2>&1
  check_result $?

  echo -n ">> Add nginx package repository source ... "
  echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] http://nginx.org/packages/mainline/ubuntu `lsb_release -cs` nginx" | sudo tee /etc/apt/sources.list.d/nginx.list > /dev/null 2>&1
  check_result $?

  echo -n ">> Update package repositories ... "
  sudo apt update

  sudo apt-mark unhold nginx > /dev/null 2>&1
  _nginx_version=$(sudo apt-cache madison nginx | grep 1.25.2 | awk -F '|' '{print $2}' | xargs)
  echo -n ">> Install nginx version $_nginx_version ... "
  sudo apt install --allow-downgrades -q -y nginx=$_nginx_version

  # Download custom nginx
  echo -n ">> Download and extract custom nginx ... "
  curl -kfsSL https://kb.sdi.one.th/repo/files/nginx/nginx-$_ubuntu_release.tar.gz | sudo tar xz --directory /tmp > /dev/null 2>&1
  check_result $?

  echo -n ">> Move extract nginx binary to execute dir ... "
  sudo mv /usr/sbin/nginx /usr/sbin/nginx.bak > /dev/null 2>&1
  sudo mv /tmp/nginx /usr/sbin/ > /dev/null 2>&1
  check_result $?

  if [ ! -d /var/lib/nginx ]; then
    echo -n ">> Create nginx temp dir ... "
    sudo mkdir -p /var/lib/nginx > /dev/null 2>&1
    check_result $?
  fi

  # Hold nginx version
  echo -n ">> Hold nginx version to prevent nginx update ... "
  sudo apt-mark hold nginx > /dev/null 2>&1
  check_result $?

  echo -n ">> Generate DH-Parameter ... "
  sudo openssl dhparam -out /etc/nginx/dh2048.pem 2048

  [ ! -d /etc/nginx/ssl ] && sudo mkdir -p /etc/nginx/ssl
  echo -n ">> Generate nginx self signed cert ... "
  sudo openssl req -x509 -newkey rsa:4096 \
    -keyout /etc/nginx/ssl/nginx-selfsigned.key \
    -out /etc/nginx/ssl/nginx-selfsigned.crt -sha256 -days 3650 -nodes \
    -subj "/C=TH/ST=Bangkok/L=Huaikwang/O=Internet Thailand PCL/OU=INET-SE/CN=$(hostname)" > /dev/null 2>&1
  check_result $?

  echo -n ">> Download nginx.conf template file ... "
  curl -kfsSL https://kb.sdi.one.th/repo/files/nginx/nginx.conf | sudo tee /etc/nginx/nginx.conf > /dev/null 2>&1
  check_result $?

  echo -n ">> Download default nginx config file ... "
  curl -kfsSL https://kb.sdi.one.th/repo/files/nginx/000_default.conf | sudo tee /etc/nginx/conf.d/000_default.conf > /dev/null 2>&1
  check_result $?

  if [ -f /etc/nginx/conf.d/default.conf ]; then
    echo -n ">> Remove package pre-build default nginx config ... "
    sudo rm -rf /etc/nginx/conf.d/default.conf > /dev/null 2>&1
    check_result $?
  fi

  # Set limit nofile to nginx service user
  if ! grep -Fxq "### Auto add nginx limit nofile ###" /etc/security/limits.conf; then
    echo -n ">> Set limit nofile for nginx service user ... "
    cat <<EOF | sudo tee -a /etc/security/limits.conf > /dev/null 2>&1
### Auto add nginx limit nofile ###
www-data        soft    nofile  65535
www-data        hard    nofile  65535
EOF
    check_result $?
  fi

  echo -n ">> Enable nginx service to startup ... "
  sudo systemctl enable nginx > /dev/null 2>&1
  check_result $?
fi


# Selected nfs_server
if [ $_nfs_server -eq 1 ]; then
  echo "============ Install NFS Server: $_nfs_dir to $_nfs_net ============"
  sudo systemctl status nfs-server > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo ">> Install nfs server service ... "
    sudo apt update && sudo apt install -y nfs-kernel-server

    echo -n ">> Enable nfs server service ... "
    sudo systemctl enable nfs-server > /dev/null 2>&1
    check_result $?

    echo -n ">> Start nfs server service ... "
    sudo systemctl start nfs-server > /dev/null 2>&1
    check_result $?
  fi

  echo -n ">> Change NFS dir owner ... "
  sudo chown nobody:nogroup $_nfs_dir > /dev/null 2>&1
  check_result $?

  echo -n ">> Change NFS dir mod ... "
  sudo chmod -R 777 $_nfs_dir > /dev/null 2>&1
  check_result $?

  echo -n ">> Export target dir to exportfs ... "
  [ ! -f /etc/exports ] && sudo touch /etc/exports > /dev/null 2>&1
  if ! grep -Fq "### Automate add $_nfs_dir to $_nfs_net ###" /etc/exports; then
    cat <<EOF | sudo tee -a /etc/exports > /dev/null 2>&1
### Automate add $_nfs_dir to $_nfs_net ###
$_nfs_dir $_nfs_net(rw,sync,no_subtree_check,insecure,no_root_squash,no_all_squash)
EOF
    check_result $?
  else 
    echo "ok"
  fi

  echo -n ">> Exporting NFS directory ... "
  sudo exportfs -a > /dev/null 2>&1
  check_result $?
fi

if [ $_mariadb -eq 1 ]; then
  echo "============ Install MariaDB Server 10.6 ============"
  sudo systemctl status mysql > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo -n ">> Add mariadb gpg repository ... "
    curl -fsSL https://supplychain.mariadb.com/mariadb-keyring-2019.gpg | sudo tee /usr/share/keyrings/mariadb-keyring-2019.gpg > /dev/null 2>&1
    check_result $?

    echo -n ">> Add mariadb server 10.6 package list ... "
    echo "deb [signed-by=/usr/share/keyrings/mariadb-keyring-2019.gpg] https://mirror.kku.ac.th/mariadb/repo/10.6/ubuntu `lsb_release -cs` main" | sudo tee /etc/apt/sources.list.d/mariadb.list > /dev/null 2>&1
    check_result $?

    # Add custom configuration
    echo -n ">> Add server configuration ... "
    curl -kfsSL https://kb.sdi.one.th/repo/files/mariadb/50-server.cnf | sed "s/<BUFFER_POOL_SIZE>/$_mariadb_innodb_cache/g" | sudo tee /etc/mysql/mariadb.conf.d/50-server.cnf > /dev/null 2>&1
    check_result $?

    if [ $_mariadb_galera_on -eq 1 ]; then
      echo -n ">> Add galera cluster configure ... "
      curl -kfsSL https://kb.sdi.one.th/repo/files/mariadb/60-galera.cnf | sed "s/<WSREP_CLUSTER_NAME>/$_mariadb_galera_cluster_name/g" | sudo tee /etc/mysql/mariadb.conf.d/60-galera.cnf > /dev/null 2>&1
      check_result $?
    fi

    # Enable and start mysql service
    echo -n ">> Enable mysql service on startup ... "
    sudo systemctl enable mysql > /dev/null 2>&1
    check_result $?

    echo -n ">> Start mysql service ... "
    sudo systemctl start mysql > /dev/null 2>&1
    check_result $?
  else
    echo "already running"
  fi
fi

# Tidy unuse package
echo ">> Tidy unuse apt package ... "
sudo apt autoremove -y

# Finished script and stamp time
FinishedTime="$(date -u +%s.%N)"
# Calculate Elapsed time
Elapsed="$(bc <<<"$FinishedTime-$StartTime")"

echo "============ Everything is Success in $Elapsed second(s) : Rebooting system ============"
if [ $_reboot -eq 1 ]; then
  sleep 5
  sudo reboot
fi
