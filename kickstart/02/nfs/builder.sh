#!/bin/bash
echo "============ NFS StorageClass ============"
_template_file="https://kb.sdi.one.th/repo/files/k8s/nfs/nfs.yml"

create_ns() {
  kubectl get namespace $1 > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    kubectl create namespace $1 > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo Create new namespace failed
      exit 1
    fi
    echo ">> Create new namespace $1"
  fi
}

# Ask for parameter
_ns=nfs
_nfs_server=
_nfs_dir=
_storage_class_name=nfs-client
while true; do
  read -p "Select namespace [default: nfs] " input
  [ ! $input ] && input=nfs
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _ns=$input
    break
  else
    echo Invalid namespace name, try again
  fi
done

read -p "NFS Server [default: 192.168.1.11] " input
[ ! $input ] && input="192.168.1.11"
_nfs_server=$input

read -p "NFS dir [default: /storage] " input
[ ! $input ] && input="/storage"
_nfs_dir=$input

while true; do
  read -p "Select storage class name [default: nfs-client] " input
  [ ! $input ] && input=nfs-client
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _storage_class_name=$input
    break
  else
    echo Invalid storage class name, try again
  fi
done

_nfs_dir=$(echo $_nfs_dir | sed -e "s#/#\\\/#g")
# Generate deployment
_config=$(curl -kfsSL $_template_file \
  | sed "s/<NAMESPACE>/$_ns/g" \
  | sed "s/<NFS_SERVER>/$_nfs_server/g" \
  | sed "s/<NFS_DIR>/$_nfs_dir/g" \
  | sed "s/<STORAGE_CLASS_NAME>/$_storage_class_name/g"
)
echo ""
echo "============ Start: Output YAML file ============"
echo "$_config"
echo "============ End: Output YAML file ============"

_deploy=0
while true; do
  read -p "Do you want to automate deploy? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _deploy=1;
      break;;
    [nN] ) _deploy=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

# Auto deploy
if [ $_deploy -eq 1 ]; then
  # Check kubectl cmd
  kubectl version --short > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo kubectl command not found.
    exit 1
  fi
  echo "Start deploy ... "
  create_ns $_ns
  echo "$_config" | kubectl apply -f -
  if [ $? -eq 0 ]; then
    echo ">> Deploy success ..."
  else
    echo ">> Deploy failed !!!"
  fi
  echo ""
fi

# save to file
read -p "What is your destination filename? [default: $_ns-$_deployment_name.yml] " input
[ ! $input ] && input=$_ns-$_deployment_name.yml
echo -n "Saving to $input ... "
echo "$_config" > $input
if [ $? -eq 0 ]; then
  echo "ok"
else
  echo "failed"
fi