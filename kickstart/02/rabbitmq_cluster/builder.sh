#!/bin/bash
echo "============ RabbitMQ - Cluster ============"
_template_file="https://kb.sdi.one.th/repo/files/k8s/rabbitmq_cluster/rabbitmq.yml"

create_ns() {
  kubectl get namespace $1 > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    kubectl create namespace $1 > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo Create new namespace failed
      exit 1
    fi
    echo ">> Create new namespace $1"
  fi
}

_ns=msg-queue
_deployment_name=rabbitmq
_mgmt_node_port=30552

_pvc_name=$_deployment_name
_storage_class_name=nfs-client
_storage_size="50Mi"

# Namespace
while true; do
  read -p "Select namespace [default: $_ns] " input
  [ ! $input ] && input=$_ns
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _ns=$input
    break
  else
    echo Invalid namespace name, try again
  fi
done

# Deployment name
while true; do
  read -p "Select deployment name [default: $_deployment_name] " input
  [ ! $input ] && input=$_deployment_name
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _deployment_name=$input
    break
  else
    echo Invalid deployment name, try again
  fi
done

_cluster_size=3
PS3="Select RabbitMQ cluster size (Depend on number of worker): "
select _s in "3 nodes" "5 nodes" Quit; do
  case $REPLY in
    "1") _cluster_size=3; break;;
    "2") _cluster_size=5; break;;
    $((${#_items[@]}+1))) echo ""; echo "bye bye."; exit 0;;
    *) echo Unknown choice;;
  esac
done

# node port
while true; do
  read -p "Select node port [default: $_mgmt_node_port] " input
  [ ! $input ] && input=$_mgmt_node_port
  if [[ $input =~ ^[0-9]+$ ]]; then
    _mgmt_node_port=$input
    break
  else
    echo Invalid node port, try again
  fi
done

_def_pvc=$(cat <<EOF
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: data-<DEPLOYMENT_NAME>-<NODE_NUMBER>
  namespace: <NAMESPACE>
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: <STORAGE_SIZE>
  storageClassName: <STORAGE_CLASS_NAME>
  volumeMode: Filesystem
EOF
)

_pvc_create=$(
  for (( i=0; i<$_cluster_size; i++ )); do
    _s=$(echo "$_def_pvc" \
      | sed "s/<NAMESPACE>/$_ns/g" \
      | sed "s/<DEPLOYMENT_NAME>/$_deployment_name/g" \
      | sed "s/<NODE_NUMBER>/$i/g" \
      | sed "s/<STORAGE_SIZE>/$_storage_size/g" \
      | sed "s/<STORAGE_CLASS_NAME>/$_storage_class_name/g"
    )
    echo "$_s"
  done
)

# Read template and apply config
# Generate deployment
_config=$(curl -kfsSL $_template_file \
  | sed "s/<NAMESPACE>/$_ns/g" \
  | sed "s/<DEPLOYMENT_NAME>/$_deployment_name/g" \
  | sed "s/<NODE_PORT>/$_mgmt_node_port/g" \
  | sed "s/<PVC_SIZE>/$_storage_size/g" \
  | sed "s/<STORAGE_CLASS_NAME>/$_storage_class_name/g" \
  | sed "s/<CLUSTER_SIZE>/$_cluster_size/g"
)

echo ""
echo "============ Start: Output YAML file ============"
echo "$_config"
echo "============ End: Output YAML file ============"

_deploy=0
while true; do
  read -p "Do you want to automate deploy? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _deploy=1;
      break;;
    [nN] ) _deploy=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

# Auto deploy
if [ $_deploy -eq 1 ]; then
  # Check kubectl cmd
  kubectl version --short > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo kubectl command not found.
    exit 1
  fi
  echo "Start deploy ... "
  create_ns $_ns
  echo "$_config" | kubectl create -f -
  if [ $? -eq 0 ]; then
    echo ">> Deploy success ..."
  else
    echo ">> Deploy failed !!!"
  fi
  echo ""
fi

# update deployment file with pvc
_config=$(echo "$_config
$_pvc_create")

# save to file
read -p "What is your destination filename? [default: $_ns-$_deployment_name.yml] " input
[ ! $input ] && input=$_ns-$_deployment_name.yml
echo -n "Saving to $input ... "
echo "$_config" > $input
if [ $? -eq 0 ]; then
  echo "ok"
else
  echo "failed"
fi