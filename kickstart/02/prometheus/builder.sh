#!/bin/bash
echo "============ Prometheus Monitor ============"
_template_file="https://kb.sdi.one.th/repo/files/k8s/prometheus/prometheus.yml"

create_ns() {
  kubectl get namespace $1 > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    kubectl create namespace $1 > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo Create new namespace failed
      exit 1
    fi
    echo ">> Create new namespace $1"
  fi
}

_ns=prometheus
_deployment_name=prometheus
_pod_name=prometheus
_node_port=30550
_route_prefix=/
# Namespace
while true; do
  read -p "Select namespace [default: prometheus] " input
  [ ! $input ] && input=prometheus
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _ns=$input
    break
  else
    echo Invalid namespace name, try again
  fi
done

# Deployment name
while true; do
  read -p "Select deployment name [default: prometheus] " input
  [ ! $input ] && input=prometheus
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _deployment_name=$input
    break
  else
    echo Invalid deployment name, try again
  fi
done

# Pod name
# while true; do
#   read -p "Select pod name [default: prometheus] " input
#   [ ! $input ] && input=prometheus
#   if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
#     _pod_name=$input
#     break
#   else
#     echo Invalid pod name, try again
#   fi
# done

# node port
while true; do
  read -p "Select node port [default: 30550] " input
  [ ! $input ] && input=30550
  if [[ $input =~ ^[0-9]+$ ]]; then
    _node_port=$input
    break
  else
    echo Invalid node port, try again
  fi
done

# Prometheus route prefix
read -p "Prometheus route prefix [default: /] " input
[ ! $input ] && input="/"
_route_prefix=$input

# Read template and apply config
_route_prefix=$(echo $_route_prefix | sed -e "s#/#\\\/#g")
# Generate deployment
_config=$(curl -kfsSL $_template_file \
  | sed "s/<NAMESPACE>/$_ns/g" \
  | sed "s/<DEPLOYMENT_NAME>/$_deployment_name/g" \
  | sed "s/<POD_NAME>/$_pod_name/g" \
  | sed "s/<NODE_PORT>/$_node_port/g" \
  | sed "s/<ROUTE_PREFIX>/$_route_prefix/g" 
)

echo ""
echo "============ Start: Output YAML file ============"
echo "$_config"
echo "============ End: Output YAML file ============"

_deploy=0
while true; do
  read -p "Do you want to automate deploy? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _deploy=1;
      break;;
    [nN] ) _deploy=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

# Auto deploy
if [ $_deploy -eq 1 ]; then
  # Check kubectl cmd
  kubectl version --short > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo kubectl command not found.
    exit 1
  fi
  echo "Start deploy ... "
  create_ns $_ns
  echo "$_config" | kubectl apply -f -
  if [ $? -eq 0 ]; then
    echo ">> Deploy success ..."
  else
    echo ">> Deploy failed !!!"
  fi
  echo ""
fi

# save to file
read -p "What is your destination filename? [default: $_ns-$_deployment_name.yml] " input
[ ! $input ] && input=$_ns-$_deployment_name.yml
echo -n "Saving to $input ... "
echo "$_config" > $input
if [ $? -eq 0 ]; then
  echo "ok"
else
  echo "failed"
fi