#!/bin/bash
COLUMNS=272
# Item defined
_items=(
  "NFS StorageClass"
  "Redis - Standalone"
  "Redis - Sentinel (Required: PVC)"
  "RabbitMQ - Standalone (Required: PVC)"
  "RabbitMQ - Cluster (Required: PVC)"
  "Prometheus monitor"
)
_builder=
echo "============ INET-SE - Kubenetes config builder ============"
PS3="Select deployment: "
select _s in "${_items[@]}" Quit; do
  case $REPLY in
    "1") _builder="nfs"; break;;
    "2") _builder="redis_alone"; break;;
    "3") _builder="redis_sentinel"; break;;
    "4") _builder="rabbitmq_alone"; break;;
    "5") _builder="rabbitmq_cluster"; break;;
    "6") _builder="prometheus"; break;;
    $((${#_items[@]}+1))) echo ""; echo "bye bye."; exit 0;;
    *) echo Unknown choice;;
  esac
done

echo ""
if [ $_builder ]; then
  curl -IHEAD -kfsSL https://kb.sdi.one.th/repo/files/k8s/$_builder/builder.sh > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo Script not ready, try another choice.
    exit 2
  fi
  bash <(curl -kfsSL https://kb.sdi.one.th/repo/files/k8s/$_builder/builder.sh)
else
  echo Something error, bye bye.
fi