#!/bin/bash
echo "============ Redis - Standalone node ============"
_template_file="https://kb.sdi.one.th/repo/files/k8s/redis_alone/redis.yml"
_template_no_pvc_file="https://kb.sdi.one.th/repo/files/k8s/redis_alone/redis_no_pvc.yml"

create_ns() {
  kubectl get namespace $1 > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    kubectl create namespace $1 > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo Create new namespace failed
      exit 1
    fi
    echo ">> Create new namespace $1"
  fi
}

_ns=caching
_deployment_name=redis-alone
_pod_name=redis
_redis_password=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 16 ; echo '')

_pvc_on=1
_pvc_name=$_deployment_name
_storage_class_name=nfs-client
_storage_size="2Gi"

# Namespace
while true; do
  read -p "Select namespace [default: $_ns] " input
  [ ! $input ] && input=$_ns
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _ns=$input
    break
  else
    echo Invalid namespace name, try again
  fi
done

# Deployment name
while true; do
  read -p "Select deployment name [default: $_deployment_name] " input
  [ ! $input ] && input=$_deployment_name
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _deployment_name=$input
    break
  else
    echo Invalid deployment name, try again
  fi
done

_redis_ver=v7
PS3="Select Redis version: "
select _s in "v5" "v7" Quit; do
  case $REPLY in
    "1") _redis_ver="5-alpine3.16"; break;;
    "2") _redis_ver="7-alpine3.18"; break;;
    $((${#_items[@]}+1))) echo ""; echo "bye bye."; exit 0;;
    *) echo Unknown choice;;
  esac
done

# Redis password
read -p "Specific redis password [default: auto-gen] " input
[ ! $input ] && input=$_redis_password; echo "Auto generate redis password: "$_redis_password
_redis_password=$input

while true; do
  read -p "Do you want to use PVC storage? (y/n) [default: y] " yn
  [ ! $yn ] && yn=y
  case $yn in 
    [yY] ) _pvc_on=1;
      break;;
    [nN] ) _pvc_on=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

if [ $_pvc_on -eq 1 ]; then
  # Generate deployment
  _config=$(curl -kfsSL $_template_file \
    | sed "s/<NAMESPACE>/$_ns/g" \
    | sed "s/<DEPLOYMENT_NAME>/$_deployment_name/g" \
    | sed "s/<POD_NAME>/$_pod_name/g" \
    | sed "s/<REDIS_PASSWORD>/$_redis_password/g" \
    | sed "s/<REDIS_VERSION>/$_redis_ver/g" \
    | sed "s/<PVC_SIZE>/$_storage_size/g" \
    | sed "s/<STORAGE_CLASS_NAME>/$_storage_class_name/g" \
    | sed "s/<PVC_NAME>/$_pvc_name/g"
  )
else
  # Generate deployment
  _config=$(curl -kfsSL $_template_no_pvc_file \
    | sed "s/<NAMESPACE>/$_ns/g" \
    | sed "s/<DEPLOYMENT_NAME>/$_deployment_name/g" \
    | sed "s/<POD_NAME>/$_pod_name/g" \
    | sed "s/<REDIS_PASSWORD>/$_redis_password/g" \
    | sed "s/<REDIS_VERSION>/$_redis_ver/g"
  )
fi

echo ""
echo "============ Start: Output YAML file ============"
echo "$_config"
echo "============ End: Output YAML file ============"

_deploy=0
while true; do
  read -p "Do you want to automate deploy? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _deploy=1;
      break;;
    [nN] ) _deploy=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

# Auto deploy
if [ $_deploy -eq 1 ]; then
  # Check kubectl cmd
  kubectl version --short > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo kubectl command not found.
    exit 1
  fi
  echo ">> Start deploy ... "
  # Create namespace
  create_ns $_ns
  echo "$_config" | kubectl apply -f -
  if [ $? -eq 0 ]; then
    echo ">> Deploy success ..."
  else
    echo ">> Deploy failed !!!"
  fi
  echo ""
fi

# save to file
read -p "What is your destination filename? [default: $_ns-$_deployment_name.yml] " input
[ ! $input ] && input=$_ns-$_deployment_name.yml
echo -n "Saving to $input ... "
echo "$_config" > $input
if [ $? -eq 0 ]; then
  echo "ok"
else
  echo "failed"
fi