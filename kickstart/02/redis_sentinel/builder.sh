#!/bin/bash
echo "============ Redis - Sentinel ============"
_template_pre_file="https://kb.sdi.one.th/repo/files/k8s/redis_sentinel/preconfig.yml"
_template_statefule_file="https://kb.sdi.one.th/repo/files/k8s/redis_sentinel/statefulset.yml"
_template_haproxy_init="https://kb.sdi.one.th/repo/files/k8s/redis_sentinel/haproxy-init.txt"

create_ns() {
  kubectl get namespace $1 > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    kubectl create namespace $1 > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo Create new namespace failed
      exit 1
    fi
    echo ">> Create new namespace $1"
  fi
}

_ns=caching
_deployment_name=redis-ha
_redis_password=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 16 ; echo '')

_pvc_name=$_deployment_name
_storage_class_name=nfs-client
_storage_size="2Gi"

# Namespace
while true; do
  read -p "Select namespace [default: $_ns] " input
  [ ! $input ] && input=$_ns
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _ns=$input
    break
  else
    echo Invalid namespace name, try again
  fi
done

# Deployment name
while true; do
  read -p "Select deployment name [default: $_deployment_name] " input
  [ ! $input ] && input=$_deployment_name
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _deployment_name=$input
    break
  else
    echo Invalid deployment name, try again
  fi
done

_redis_ver=v7
_sentinel_size=3
_sentinel_quorum=2
PS3="Select Redis version: "
select _s in "v5" "v7" Quit; do
  case $REPLY in
    "1") _redis_ver="5-alpine3.16"; break;;
    "2") _redis_ver="7-alpine3.18"; break;;
    $((${#_items[@]}+1))) echo ""; echo "bye bye."; exit 0;;
    *) echo Unknown choice;;
  esac
done

PS3="Select Redis sentinel size (Depend on number of worker): "
select _s in "3 nodes" "5 nodes" Quit; do
  case $REPLY in
    "1") _sentinel_size=3; _sentinel_quorum=2; break;;
    "2") _sentinel_size=5; _sentinel_quorum=3; break;;
    $((${#_items[@]}+1))) echo ""; echo "bye bye."; exit 0;;
    *) echo Unknown choice;;
  esac
done

# Redis password
read -p "Specific redis password [default: auto-gen] " input
[ ! $input ] && input=$_redis_password; echo -n "Auto generate redis password: "$_redis_password
_redis_password=$(echo -n $input | base64)

_init_script=$(curl -kfsSL $_template_haproxy_init)
_haproxy_init_script=$(
  for (( i=0; i<$_sentinel_size; i++ )); do
    echo "$_init_script" \
      | sed "s/<DEPLOYMENT_NAME>/$_deployment_name/g" \
      | sed "s/<NODE_NUMBER>/$i/g"
  done
)

_haproxy_backend_check=$(
  for (( i=0; i<$_sentinel_size; i++ )); do
    _s=$(echo "    # Check Sentinel and whether they are nominated master
    backend check_if_redis_is_master_$i
      mode tcp
      option tcp-check
      tcp-check connect
      tcp-check send PING\r\n
      tcp-check expect string +PONG
      tcp-check send SENTINEL\ get-master-addr-by-name\ $_deployment_name-master\r\n
      tcp-check expect string REPLACE_ANNOUNCE$i
      tcp-check send QUIT\r\n
      tcp-check expect string +OK"
      for (( j=0; j<$_sentinel_size; j++ )); do
        echo "      server R$j $_deployment_name-announce-$j:26379 check inter 1s"
      done
    )
    echo "$_s"
  done
)

_haproxy_master_backend=$(
  for (( i=0; i<$_sentinel_size; i++ )); do
    echo "      use-server R$i if { srv_is_up(R$i) } { nbsrv(check_if_redis_is_master_$i) ge 2 }
      server R$i $_deployment_name-announce-$i:6379 check inter 1s fall 1 rise 1"
  done
)

_redis_service=$(cat <<EOF
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: <DEPLOYMENT_NAME>
    chart: <DEPLOYMENT_NAME>-4.4.2
    heritage: Tiller
    release: <DEPLOYMENT_NAME>
  name: <DEPLOYMENT_NAME>-announce-<NODE_NUMBER>
  namespace: <NAMESPACE>
spec:
  ports:
  - name: server
    port: 6379
    protocol: TCP
    targetPort: redis
  - name: sentinel
    port: 26379
    protocol: TCP
    targetPort: sentinel
  publishNotReadyAddresses: true
  selector:
    app: <DEPLOYMENT_NAME>
    release: <DEPLOYMENT_NAME>
    statefulset.kubernetes.io/pod-name: <DEPLOYMENT_NAME>-server-<NODE_NUMBER>
  type: ClusterIP
EOF
)
_redis_pvc=$(cat <<EOF
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: data-<DEPLOYMENT_NAME>-server-<NODE_NUMBER>
  namespace: <NAMESPACE>
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: <STORAGE_SIZE>
  storageClassName: <STORAGE_CLASS_NAME>
  volumeMode: Filesystem
EOF
)

_sentinel_ids=(
"        - name: SENTINEL_ID_0\n          value: 79f3945a5a36c7748933e2cf51da3aa6b9e40022\n"
"        - name: SENTINEL_ID_1\n          value: 930dc785e368d4d0e7a7fd422489df437cdb5255\n"
"        - name: SENTINEL_ID_2\n          value: 16f18bf4f68e622c08ce79cce6e1acc8aa39bb92"
"\n        - name: SENTINEL_ID_3\n          value: da0565af12babb940965816b931e2c6362220dbc\n"
"        - name: SENTINEL_ID_4\n          value: 610614bf1b3cef52b3204c13ef94b2ba8da7f5ed"
)

_service_config=$(
  for (( i=0; i<$_sentinel_size; i++ )); do
    _s=$(echo "$_redis_service" \
      | sed "s/<NAMESPACE>/$_ns/g" \
      | sed "s/<DEPLOYMENT_NAME>/$_deployment_name/g" \
      | sed "s/<NODE_NUMBER>/$i/g"
    )
    echo "$_s"
  done
)

_pvc_create=$(
  for (( i=0; i<$_sentinel_size; i++ )); do
    _s=$(echo "$_redis_pvc" \
      | sed "s/<NAMESPACE>/$_ns/g" \
      | sed "s/<DEPLOYMENT_NAME>/$_deployment_name/g" \
      | sed "s/<NODE_NUMBER>/$i/g" \
      | sed "s/<STORAGE_SIZE>/$_storage_size/g" \
      | sed "s/<STORAGE_CLASS_NAME>/$_storage_class_name/g"
    )
    echo "$_s"
  done
)

_sentinel_id=$(
  for (( i=0; i<$_sentinel_size; i++ )); do
    echo -n "${_sentinel_ids[$i]}"
  done
)

# Prepare preconfig
_pre_config=$(curl -kfsSL $_template_pre_file \
  | sed "s/<NAMESPACE>/$_ns/g" \
  | sed "s/<DEPLOYMENT_NAME>/$_deployment_name/g" \
  | sed "s/<SENTINEL_QUORUM>/$_sentinel_quorum/g" \
  | sed "s/<REDIS_AUTH>/$_redis_password/g" \
  | sed "s/<SENTINEL_SIZE>/$_sentinel_size/g"
)
# Append haproxy script
_pre_config=$(echo "${_pre_config/"<HAPROXY_BACKEND_CHECK>"/"$_haproxy_backend_check"}")
_pre_config=$(echo "${_pre_config/"<HAPROXY_MASTER_BACKEND>"/"$_haproxy_master_backend"}")
_pre_config=$(echo "${_pre_config/"<HAPROXY_INIT_CONFIG>"/"$_haproxy_init_script"}")

_stateful_config=$(curl -kfsSL $_template_statefule_file \
  | sed "s/<NAMESPACE>/$_ns/g" \
  | sed "s/<DEPLOYMENT_NAME>/$_deployment_name/g" \
  | sed "s/<REDIS_VERSION>/$_redis_ver/g" \
  | sed "s/<STORAGE_SIZE>/$_storage_size/g" \
  | sed "s/<STORAGE_CLASS_NAME>/$_storage_class_name/g" \
  | sed "s/<SENTINEL_SIZE>/$_sentinel_size/g" \
  | sed "s/<SENTINEL_IDS>/$_sentinel_id/g"
)

_config=$(echo "$_pre_config
$_service_config
---
$_stateful_config")

echo ""
echo "============ Start: Output YAML file ============"
echo "$_config"
echo "============ End: Output YAML file ============"

_deploy=0
while true; do
  read -p "Do you want to automate deploy? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _deploy=1;
      break;;
    [nN] ) _deploy=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

# Auto deploy
if [ $_deploy -eq 1 ]; then
  # Check kubectl cmd
  kubectl version --short > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo kubectl command not found.
    exit 1
  fi
  echo "Start deploy ... "
  create_ns $_ns
  echo "$_config" | kubectl create -f -
  if [ $? -eq 0 ]; then
    echo ">> Deploy success ..."
  else
    echo ">> Deploy failed !!!"
  fi
  echo ""
fi

# update deployment file with pvc
_config=$(echo "$_config
$_pvc_create")

# save to file
read -p "What is your destination filename? [default: $_ns-$_deployment_name.yml] " input
[ ! $input ] && input=$_ns-$_deployment_name.yml
echo -n "Saving to $input ... "
echo "$_config" > $input
if [ $? -eq 0 ]; then
  echo "ok"
else
  echo "failed"
fi