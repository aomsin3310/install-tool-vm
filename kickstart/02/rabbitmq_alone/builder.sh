#!/bin/bash
echo "============ RabbitMQ - Standalone node ============"
_template_file="https://kb.sdi.one.th/repo/files/k8s/rabbitmq_alone/rabbitmq.yml"

create_ns() {
  kubectl get namespace $1 > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    kubectl create namespace $1 > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo Create new namespace failed
      exit 1
    fi
    echo ">> Create new namespace $1"
  fi
}

_ns=rabbitmq
_deployment_name=rabbitmq
_pod_name=rabbitmq
_mgmt_node_port=30551
_pvc_name=$_deployment_name
_storage_class_name=nfs-client
_storage_size="1Gi"

# Namespace
while true; do
  read -p "Select namespace [default: $_ns] " input
  [ ! $input ] && input=$_ns
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _ns=$input
    break
  else
    echo Invalid namespace name, try again
  fi
done

# Deployment name
while true; do
  read -p "Select deployment name [default: $_deployment_name] " input
  [ ! $input ] && input=$_deployment_name
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _deployment_name=$input
    _pvc_name=$_deployment_name
    break
  else
    echo Invalid deployment name, try again
  fi
done

# storage class name
while true; do
  read -p "Select storage class name [default: $_storage_class_name] " input
  [ ! $input ] && input=$_storage_class_name
  if [[ $input =~ ^[a-zA-Z0-9\-]+$ ]]; then
    _storage_class_name=$input
    break
  else
    echo Invalid storage class name, try again
  fi
done

# node port
while true; do
  read -p "Select node port [default: $_mgmt_node_port] " input
  [ ! $input ] && input=$_mgmt_node_port
  if [[ $input =~ ^[0-9]+$ ]]; then
    _mgmt_node_port=$input
    break
  else
    echo Invalid node port, try again
  fi
done

# Read template and apply config
# Generate deployment
_config=$(curl -kfsSL $_template_file \
  | sed "s/<NAMESPACE>/$_ns/g" \
  | sed "s/<DEPLOYMENT_NAME>/$_deployment_name/g" \
  | sed "s/<POD_NAME>/$_pod_name/g" \
  | sed "s/<NODE_PORT>/$_mgmt_node_port/g" \
  | sed "s/<PVC_NAME>/$_pvc_name/g" \
  | sed "s/<PVC_SIZE>/$_storage_size/g" \
  | sed "s/<STORAGE_CLASS_NAME>/$_storage_class_name/g" 
)

echo ""
echo "============ Start: Output YAML file ============"
echo "$_config"
echo "============ End: Output YAML file ============"

_deploy=0
while true; do
  read -p "Do you want to automate deploy? (y/n) [default: n] " yn
  [ ! $yn ] && yn=n
  case $yn in 
    [yY] ) _deploy=1;
      break;;
    [nN] ) _deploy=0;
      break;;
    * ) echo invalid response;
      exit 1;;
  esac
done

# Auto deploy
if [ $_deploy -eq 1 ]; then
  # Check kubectl cmd
  kubectl version --short > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo kubectl command not found.
    exit 1
  fi
  echo "Start deploy ... "
  create_ns $_ns
  echo "$_config" | kubectl apply -f -
  if [ $? -eq 0 ]; then
    echo ">> Deploy success ..."
  else
    echo ">> Deploy failed !!!"
  fi
  echo ""
fi

# save to file
read -p "What is your destination filename? [default: $_ns-$_deployment_name.yml] " input
[ ! $input ] && input=$_ns-$_deployment_name.yml
echo -n "Saving to $input ... "
echo "$_config" > $input
if [ $? -eq 0 ]; then
  echo "ok"
else
  echo "failed"
fi