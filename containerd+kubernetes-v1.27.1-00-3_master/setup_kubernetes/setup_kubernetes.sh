# Disable swap
sudo swapoff -a
sudo rm /swap.img
sed -ri '/\sswap\s/s/^#?/#/' /etc/fstab

sleep 2

cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

sleep 2

sudo modprobe overlay
sleep 2

sudo modprobe br_netfilter
sleep 2

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
sleep 2

sudo sysctl --system
sleep 2

sudo apt update
sleep 2
sudo apt -y upgrade
sleep 2

# Install containerd
sudo apt install -y containerd
sleep 2
sudo systemctl start containerd
sleep 2
sudo systemctl enable containerd
sleep 2

#install kubernetes
sudo apt-get update 
sleep 2
sudo apt-get install -y apt-transport-https ca-certificates curl
sleep 2

sudo mkdir -p /etc/apt/keyrings 
sleep 2
curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-archive-keyring.gpg
sleep 2

echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sleep 2

sudo apt-get update
sleep 2
sudo apt-get install -y --allow-change-held-packages kubelet=1.27.1-00 kubeadm=1.27.1-00 kubectl=1.27.1-00
sleep 2
sudo apt-mark hold kubelet kubeadm kubectl
sleep 2

sudo ufw allow 6443
sleep 2
sudo ufw allow 6443/tcp
sleep 2

# Install Nginx
sudo apt install -y nginx
sleep 2

# Start Nginx and enable it to start on boot
sudo systemctl start nginx
sleep 2
sudo systemctl enable nginx
sleep 2

# Display Nginx status

# Configure Nginx as a load balancer for Kubernetes API servers
cat <<EOL | sudo tee -a /etc/nginx/nginx.conf
###begin
stream {
  upstream kubernetes {
    server 139.59.104.248:6443;
    server 167.172.73.254:6443;
    server 209.97.163.52:6443;
  }
  access_log off;
  error_log /var/log/nginx/kubernetes_error.log;
  server {
    listen localhost:10443;
    proxy_pass kubernetes;
    proxy_timeout 10m;
    proxy_connect_timeout 1s;
  }
}
###end
EOL
sleep 2
# Test Nginx configuration
sudo nginx -t
sleep 2
# Restart Nginx
sudo systemctl restart nginx
sleep 2

echo "Nginx configuration completed."
sleep 2
