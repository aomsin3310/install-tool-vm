#!/bin/bash

# Define parameter
network_nfs=159.223.87.61
nfsdir=/storage

sleep 2

# Install update
sudo apt update && sudo apt install nfs-kernel-server -y
sleep 2

sudo systemctl enable nfs-server
sleep 2

sudo systemctl start nfs-server
sleep 2

sudo systemctl status nfs-server
sleep 2


# NFS Config
if [ -d "$nfsdir" ]; then
  echo "$nfsdir already exists"
else
  mkdir -p $nfsdir
  echo "create"          
fi
sleep 2

chown nobody:nogroup $nfsdir
sleep 2

chmod -R 777 $nfsdir
sleep 2

sudo tee /etc/exports <<EOF
$nfsdir $network_nfs/24(rw,sync,no_subtree_check,insecure,no_root_squash,no_all_squash)
EOF
sleep 2

# Export nfs
sudo exportfs -rav
sleep 2

sudo exportfs -v
sleep 2

showmount -e
sleep 2
