#!/bin/bash

# 1. Update & Upgrade
sudo apt update 
sleep 2

sudo apt upgrade -y
sleep 2

# 3. Enable modprobe and bridge-nf
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF
sleep 2

sudo modprobe overlay
sleep 2

sudo modprobe br_netfilter
sleep 2

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
sleep 2

sudo sysctl --system
sleep 2

# 5. Swap off
swapoff -a
sleep 2

sed -ri '/\sswap\s/s/^#?/#/' /etc/fstab
sleep 2

# 6. Sync NTP & timezone
timedatectl set-timezone Asia/Bangkok
sleep 2

apt install chrony -y
sleep 2

sed -i 's/pool/#pool/g' /etc/chrony/chrony.conf
sleep 2

sed -i 's/#pool 2.ubuntu.#pool.ntp.org iburst maxsources 2/server clock.inet.co.th/g' /etc/chrony/chrony.conf
sleep 2

systemctl restart chrony
sleep 2

# 7. Install nfs-common for storageClass NFS-Client
sudo apt install nfs-common -y
sleep 2

# 8. Install docker-ce 20.10
curl https://releases.rancher.com/install-docker/20.10.sh | sudo sh -
sleep 2
