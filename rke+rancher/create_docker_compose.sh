#!/bin/bash

# Create a directory and move into it
mkdir rancher
sleep 2

cd rancher
sleep 2

# Create docker-compose.yml file
cat <<EOF > docker-compose.yml
version: "3.7"
services:
  rancher:
    container_name: rancher-rke
    image: rancher/rancher:stable
    restart: unless-stopped
    privileged: true
    ports:
      - 80:80
      - 443:443
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ./data:/var/lib/rancher
      - ./auditlog:/var/log/auditlog
    environment:
      - AUDIT_LEVEL=1
      - CATTLE_NEW_SIGNED_CERT_EXPIRATION_DAYS=3650
EOF
sleep 2

# Install Docker Compose
sudo apt install docker-compose -y
sleep 2

# Run docker-compose up
docker-compose up -d
sleep 2

# Display running containers
docker ps
sleep 2
